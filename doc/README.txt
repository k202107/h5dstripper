ncview compatibility

1. Creation order

hid_t file_id
hid_t fcpl_id;
fcpl_id = H5Pcreate(H5P_FILE_CREATE);
H5Pset_link_creation_order(fcpl_id, H5P_CRT_ORDER_TRACKED|H5P_CRT_ORDER_INDEXED);
H5Pset_attr_creation_order(fcpl_id, H5P_CRT_ORDER_TRACKED|H5P_CRT_ORDER_INDEXED);
file_id = H5Fcreate(filename, H5F_ACC_TRUNC, fcpl_id, H5P_DEFAULT)) < 0) BANG(file);

hid_t group_id
hid_t gcpl_id;
gcpl_id = H5Pcreate(H5P_FILE_CREATE);
H5Pset_link_creation_order(gcpl_id, H5P_CRT_ORDER_TRACKED|H5P_CRT_ORDER_INDEXED);
H5Pset_attr_creation_order(gcpl_id, H5P_CRT_ORDER_TRACKED|H5P_CRT_ORDER_INDEXED);
group_id = H5Gcreate(g_root, "g1", H5P_DEFAULT, gcpl_id, H5P_DEFAULT); 


2. References to the longitude, latitude and time datasets
