
#ifndef  vector_INC
#define  vector_INC

#include <stdbool.h>

#define SVECTOR_INITIAL_CAPACITY 3;

typedef struct {
	unsigned int size;
	unsigned int capacity;
	char** data;
} svector_t;

void svector_init(svector_t* vector);
void svector_append(svector_t*, char* elem);
void svector_destroy(svector_t* vector);
void svector_copy(svector_t* target, svector_t* source);
bool svector_contains(svector_t* vector, char* str);
void svector_print(svector_t* vector);
void svector_exclude(svector_t* seta, svector_t* setb, svector_t* result);
void svector_clear(svector_t* vector);
void tokenize(svector_t* tokens, char* path, const char* delims);

#endif   /* ----- #ifndef vector_INC  ----- */
