
#ifndef  types_INC
#define  types_INC

#include <stdlib.h>
#include <stdio.h>
#include <hdf5.h>
#include "vector.h"


/**
 * @brief Iteration function parameter
 */
typedef struct {
	hid_t ifile; 	// input file
	hid_t ofile; 	// keep datasets file
	hid_t sfile; 	// external datasets file
	svector_t odsetnames;
	svector_t sdsetnames;
} visitor2_param_t;



/**
 * @brief Command line Parameter
 */
typedef struct {
	char* ifilename; 	// input file
	char* ofilename; 	// keep datasets file
	char* sfilename; 	// external datasets file
	svector_t odsetnames;
	svector_t sdsetnames;
} param_t;



void param_init(param_t* p, char* ifilename, char* ofilename, char* sfilename, svector_t odsetnames, svector_t sdsetnames);
void param_destroy(param_t* p);
void visitor2_param_init(visitor2_param_t* p, hid_t ifile, hid_t ofile, hid_t sfile, svector_t odsetnames, svector_t sdsetnames);
void visitor2_param_destroy(visitor2_param_t* p);
void param_print(param_t* p);

#endif   /* ----- #ifndef types_INC  ----- */
