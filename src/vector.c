#include "vector.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "basefuncs.h"



/**
 * @brief Initialize an empty vector.
 *
 * Don't forget to destroy the vector to avoid memory leaks.
 *
 * @param vector vector to initialize
 */
void svector_init(svector_t* vector) {
	vector->size = 0;
	vector->capacity = SVECTOR_INITIAL_CAPACITY;
	vector->data = (char**)malloc(sizeof(char*) * vector->capacity);
}



/**
 * @brief Append an element to the vector.
 *
 * @param vector target vector
 * @param elem element to append
 */
void svector_append(svector_t* vector, char* elem) {
	// Allocate more memory
	if (vector->size == vector->capacity) {
		vector->capacity = vector->capacity * 2;
		char** data = (char**)malloc(sizeof(char*) * vector->capacity);
		for (size_t i = 0; i < vector->size; ++i) {
			data[i] = vector->data[i];
		}
		free(vector->data);
		vector->data = data;
	}
	char* elem_copy = (char*)malloc(sizeof(char) * strlen(elem) + 1);
	strcpy(elem_copy, elem);
	vector->data[vector->size] = elem_copy;
	++vector->size;
}



/**
 * @brief Destroy vector.
 *
 * @param vector vector to destroy
 */
void svector_destroy(svector_t* vector) {
	for (unsigned int i = 0; i < vector->size; ++i) {
		free(vector->data[i]);
	}
	free(vector->data);
}



/**
 * @brief Create a deep copy of the source vector. 
 *
 * @param target target vector
 * @param source source vector
 */
void svector_copy(svector_t* target, svector_t* source) {
	svector_clear(target);
	for (size_t i = 0; i < source->size; ++i) {
		svector_append(target, source->data[i]);
	}
}



/**
 * @brief Check if vector contains a string
 *
 * @param vector 
 * @param str
 *
 * @return true is vector contains str, else false
 */
bool svector_contains(svector_t* vector, char* str) {
	return contains(vector->size, vector->data, str);
}



/**
 * @brief Print content of a vector
 *
 * @param vector
 */
void svector_print(svector_t* vector) {
	printf("\tvector size:\t%d\n", vector->size);
	printf("\tvector capacity:\t%d\n", vector->capacity);
	for (unsigned int i = 0; i < vector->size; ++i) {
		printf("\tvector elem: %d\t value: %s\n", i, vector->data[i]);
	}
}



/**
 * @brief Creates a new vector with
 *
 * @param seta basis vector
 * @param setb elements to remove from seta 
 * @param result seta - setb
 */
void svector_exclude(svector_t* seta, svector_t* setb, svector_t* result) {
	svector_clear(result);
	for (size_t i = 0; i < seta->size; ++i) {
		if (!svector_contains(setb, seta->data[i])) {
			svector_append(result, seta->data[i]);
		}
	}
}



/**
 * @brief Removes all element of a vector
 *
 * @param vector
 */
void svector_clear(svector_t* vector) {
	svector_destroy(vector);
	svector_init(vector);
}



/**
 * @brief Creates a vector containing group names
 *
 * @param loc
 * @param tokens
 *
 * @return 
 */
void tokenize(svector_t* tokens, char* path, const char* delims) {
//	const char* delims = "/";
	char* token = strtok(path, delims);
	while(token != NULL) {
		svector_append(tokens, token);
		token = strtok(NULL, delims); 
	}
}
