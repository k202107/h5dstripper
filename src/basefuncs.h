#ifndef  basefuncs_INC
#define  basefuncs_INC

#include <stdlib.h>
#include <stdbool.h>

bool contains(int ds_size, char** ds, char* elem);
void print_array(const int len, char** array);
int file_exist(const char *filename);

#endif   /* ----- #ifndef basefuncs_INC  ----- */
