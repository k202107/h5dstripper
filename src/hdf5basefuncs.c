#include "hdf5basefuncs.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>




/**
 * @brief 
 *
 * @param value
 *
 * @return 
 */
const char* h5o_type_to_str(H5O_type_t value) {
	static const char* h5o_unknown        = "H5O_TYPE_UNKNOWN";
	static const char* h5o_group          = "H5O_TYPE_GROUP";
	static const char* h5o_dataset        = "H5O_TYPE_DATASET";
	static const char* h5o_named_datatype = "H5O_TYPE_NAMED_DATATYPE";
	static const char* h5o_ntypes         = "H5O_TYPE_NTYPES";

	switch(value) {
		case H5O_TYPE_GROUP:
			return h5o_group;
			break;
		case H5O_TYPE_DATASET:
			return h5o_dataset;
			break;
		case H5O_TYPE_NAMED_DATATYPE:
			return h5o_named_datatype;
			break;
		case H5O_TYPE_NTYPES:
			return h5o_ntypes;
			break;
		case H5O_TYPE_UNKNOWN:
			return h5o_unknown;
			break;
	}

	return NULL;
}

/**
 * @brief 
 *
 * @param value
 *
 * @return 
 */
const char* h5i_type_to_str(H5T_class_t value) {
	static const char * h5i_file      = "H5I_FILE";
	static const char * h5i_group     = "H5I_GROUP";
	static const char * h5i_datatype  = "H5I_DATATYPE";
	static const char * h5i_dataspace = "H5I_DATASPACE";
	static const char * h5i_dataset   = "H5I_DATASET";
	static const char * h5i_attr      = "H5I_ATTR";
	static const char * h5i_badid     = "H5I_BADID";

	switch (value) {
		case H5I_FILE:
			return h5i_file;
			break;
		case H5I_GROUP:
			return h5i_group;
			break;
		case H5I_DATATYPE:
			return h5i_datatype;
			break;
		case H5I_DATASPACE:
			return h5i_dataspace;
			break;
		case H5I_DATASET:
			return h5i_dataset;
			break;
		case H5I_ATTR:
			return h5i_attr;
			break;
		case H5I_BADID:
			return h5i_badid;
			break;
	}

	return NULL;
}



/**
 * @brief Convert enum to string
 *
 * @param type
 *
 * @return 
 */
const char* h5t_class_to_str(H5T_class_t value) {
	static const char* h5t_integer   = "H5T_INTEGER";
	static const char* h5t_float     = "H5T_FLOAT";
	static const char* h5t_string    = "H5T_STRING";
	static const char* h5t_bitfield  = "H5T_BITFIELD";
	static const char* h5t_opaque    = "H5T_OPAQUE";
	static const char* h5t_compound  = "H5T_COMPOUND";
	static const char* h5t_reference = "H5T_REFERENCE";
	static const char* h5t_enum      = "H5T_ENUM";
	static const char* h5t_vlen      = "H5T_VLEN";
	static const char* h5t_array     = "H5T_ARRAY";

	switch (value) {
		case H5T_INTEGER:
			return h5t_integer;
			break;
		case H5T_FLOAT:
			return h5t_float;
			break;
		case H5T_STRING:
			return h5t_string;
			break;
		case H5T_BITFIELD:
			return h5t_bitfield;
			break;
		case H5T_OPAQUE:
			return h5t_opaque;
			break;
		case H5T_COMPOUND:
			return h5t_compound;
			break;
		case H5T_REFERENCE:
			return h5t_reference;
			break;
		case H5T_ENUM:
			return h5t_enum;
			break;
		case H5T_VLEN:
			return h5t_vlen;
			break;
		case H5T_ARRAY:   
			return h5t_array;
			break;
	}

	return NULL;
}



/**
 * @brief Converts select type to string
 *
 * @param spaceid 
 *
 * @return String representation of select type or NULL
 */
const char* h5s_sel_to_str(H5S_sel_type value) {
	static const char* h5s_sel_none       = "H5S_SEL_NONE";
	static const char* h5s_sel_points     = "H5S_SEL_POINTS";
	static const char* h5s_sel_hyperslabs = "H5S_SEL_HYPERSLABS";
	static const char* h5s_sel_all        = "H5S_SEL_ALL";

	switch (value) {
		case H5S_SEL_NONE :
			return h5s_sel_none;
			break;
		case H5S_SEL_POINTS: 
			return h5s_sel_points;
			break;
		case H5S_SEL_HYPERSLABS:
			return h5s_sel_hyperslabs;
			break;
		case H5S_SEL_ALL:
			return h5s_sel_all;
			break;
	}

	return NULL;
}



/**
 * @brief 
 *
 * @param type
 *
 * @return 
 */
const char* h5s_class_to_str(H5S_class_t value) {
	static const char* h5s_scalar = "H5S_SCALAR";
	static const char* h5s_simple = "H5S_SIMPLE";
	static const char* h5s_null   = "H5S_NULL";

	switch(value) {
		case H5S_SCALAR :
			return h5s_scalar;
			break;
		case H5S_SIMPLE: 
			return h5s_simple;
			break;
		case H5S_NULL:
			return h5s_null;
			break;
	}
	return NULL;
}



/**
 * @brief Print information about a space 
 * Format:
 * 	Select type
 * 	Class type
 * 	0 - dim size - max dim size
 * 	1 - dim size - max dim size
 * 	2 - dim size - max dim size
 * 	Number of elements in space
 *
 * @param spaceid
 */
void print_space (hid_t spaceid) {
	assert(spaceid >= 0);
	// Get dimensions
	const int ndims = H5Sget_simple_extent_ndims(spaceid);
	hsize_t dims[ndims];
	hsize_t maxdims[ndims];
	H5Sget_simple_extent_dims(spaceid, dims, maxdims);
	// Print
	printf("%s\n", h5s_sel_to_str(H5Sget_select_type(spaceid)));
	printf("%s\n", h5s_class_to_str(H5Sget_simple_extent_type(spaceid)));
	for (int i = 0; i < ndims; ++i) {
		printf("\tdim: %d, dims: %d; maxdims: %d\n", i, dims[i], maxdims[i]);
	}
	printf("\tNumber of elements: %d\n", H5Sget_simple_extent_npoints(spaceid));
}



/**
 * @brief Convert unlimited dimensions to limited ones
 *
 * @param ndims
 * @param dims
 * @param maxdims
 */
void limit_dims(const int ndims, hsize_t* dims, hsize_t* maxdims) {
	/* EB: Creation of dataset with unlimited dimensions fails. This is a temporary workaround.*/
	for (int i = 0; i < ndims; ++i) {
		if (maxdims[i] == -1) {
			maxdims[i] = dims[i];
		}
	}
}
