#ifndef  funcs_INC
#define  funcs_INC

#define MAX_NAME 64
#define NC_EHDFERR -1
#define SUCCEED 0
#define BAIL(x) exit(x)
//#define BANG(x) printf("error in %s at %d\n", __PRETTY_FUNCTION__, __LINE__); fflush(stdout); exit(x)
#define BANG(x) exit(x)

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <hdf5.h>
#include "types.h"
#include "vector.h"

void get_dataset_names(hid_t fid, svector_t* datasets);

int copy_dataset(hid_t ifid, hid_t ofid, char* dsname);
int copy_attribute (hid_t iloc, const char* iname, hid_t oloc, const char* oname, const char* name);
int fix_references (hid_t iloc, const char* iname, hid_t oloc, const char* oname, const char* name);
int fix(hid_t iloc, hid_t oloc, hid_t itype, size_t buf_size, void* ibuf, void * obuf);

int create_group_link(hid_t ofid, char* dsname, hid_t ifid, char* linkname);
int create_groups(hid_t loc, char* rel_path);
void run_stripper(param_t* p);

herr_t visitor3(hid_t gid, const char *name, const H5L_info_t *info, void *opdata);
herr_t visitor4(hid_t gid, const char *name, const H5L_info_t *info, void *opdata);
herr_t visit_attributes(hid_t oid, const char* name, const H5A_info_t* info, void* opdata);
herr_t visit_attributes_fix_refs(hid_t oid, const char* name, const H5A_info_t* info, void* opdata);
herr_t visitor_dataset_names(hid_t gid, const char *name, const H5L_info_t *info, void *opdata);
#endif   /* ----- #ifndef funcs_INC  ----- */
