#include "basefuncs.h"
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>

/**
 * @brief Print elements of an array
 *
 * @param len
 * @param array
 */
void print_array(const int len, char** array) {
	for (int i = 0; i < len; ++i) {
		printf("\telem[%d] = %s\n", i, array[i]);
	}
}



/**
 * @brief Checks if a string array contains a string;
 *
 * @param ds_size
 * @param ds
 * @param elem
 *
 * @return true if string is in the array, else false
 */
bool contains(int ds_size, char** ds, char* elem)  {
	for (int i = 0; i < ds_size; ++i) {
		if (!strcmp(elem, ds[i])) return true;
	}
	return false;
}



/**
 * @brief Checks, if an file exists
 *
 * @param filename
 *
 * @return true if the file exists, else false
 */
int does_file_exist(const char *filename) {
	struct stat st;
	int result = stat(filename, &st);
	return result == 0;
}
