/*
 * Important links:
 *
 * 1. NetCDF file format specifications:
 * https://www.unidata.ucar.edu/software/netcdf/docs/file_format_specifications.html
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <hdf5.h>
#include <argp.h>
#include "funcs.h"
#include "types.h"
#include "vector.h"



const char* argp_program_version     = "dev1";
const char* argp_program_bug_address = "betke@dkrz.de";
static char doc[]                    = "HDF5 file stripper.";
static char args_doc[]               = "";



static struct argp_option options[] = {
	{"input"   , 'i' , "filename" , 0 , "Input filename"}                                                            , 
	{"output"  , 'o' , "filename" , 0 , "Output filename"}                                                           , 
	{"shared"  , 's' , "filename" , 0 , "Shared filename"}                                                           , 
	{"keep"    , 'k' , "dsname"   , 0 , "Save this dataset (full path) in output file"}                              , 
	{"extract" , 'e' , "dsname"   , 0 , "Save this dataset (full path) in shared file and set link in output file."} , 
	{0}
};



struct arguments {
	char** args;
	char* ifilename;
	char* ofilename;
	char* sfilename;
	svector_t odsetnames;
	svector_t sdsetnames;
};



static error_t parse_opt(int key, char* arg, struct argp_state* state) {
	struct arguments* arguments = state->input;
	switch(key) {
		case 'i': 
			arguments->ifilename = arg;
			break;
		case 'o':
			arguments->ofilename = arg;
			break;
		case 's':
			arguments->sfilename = arg;
			break;
		case 'k':
			svector_append(&arguments->odsetnames, arg);
			break;
		case 'e':
			svector_append(&arguments->sdsetnames, arg);
			break;
		case ARGP_KEY_ARG:
			// do nothing
			break;
		case ARGP_KEY_END:
			// do nothing
		default:
			return ARGP_ERR_UNKNOWN;
	}
	return 0;
}



static struct argp argp = {
	options, 
	parse_opt, 
	args_doc, 
	doc
};



/*
 * GOAL:
 * Copy input file to output file including all attributes, NC4 compatible:
 * ./grid-strip -i atl*h5 -o stripped.h5 
 */
int main(int argc, char** argv) {

	// Default values	
	struct arguments arguments;
	arguments.ifilename = NULL;
	arguments.ofilename = NULL;
	arguments.sfilename = NULL;
	svector_init(&arguments.odsetnames);
	svector_init(&arguments.sdsetnames);

	argp_parse(&argp, argc, argv, 0, 0, &arguments);

	param_t params;
	param_init(
			&params, 
			arguments.ifilename, 
			arguments.ofilename, 
			arguments.sfilename, 
			arguments.odsetnames,
			arguments.sdsetnames);

	param_print(&params);
	run_stripper(&params);

	// Clean up
	svector_destroy(&arguments.odsetnames);
	svector_destroy(&arguments.sdsetnames);
	param_destroy(&params);

	return 0;
}
