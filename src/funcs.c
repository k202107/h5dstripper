#include "funcs.h"
#include "basefuncs.h"
#include "hdf5basefuncs.h"


/**
 * @brief 
 *
 * @param fid
 * @param dsetnames
 */
void get_dataset_names(hid_t fid, svector_t* dsetnames) {
	H5Lvisit(fid, H5_INDEX_CRT_ORDER, H5_ITER_NATIVE, visitor_dataset_names, (void*)dsetnames);
}



/**
 * @brief 
 *
 * @param gid
 * @param name
 * @param info
 * @param opdata
 *
 * @return 
 */
herr_t visitor_dataset_names(hid_t gid, const char *name, const H5L_info_t *info, void *opdata) {
	svector_t* dsetnames = (typeof(dsetnames))opdata;
	hid_t obj = H5Oopen(gid, name, H5P_DEFAULT);
	H5O_info_t obj_info;
	H5Oget_info(obj, &obj_info);

	ssize_t name_length  = strlen(name);
	ssize_t group_length = H5Iget_name(gid,  NULL, 0);
	char* group_name = (typeof(group_name))malloc(sizeof(*group_name) * group_length + 1);
	H5Iget_name(gid, group_name, group_length);

	char* path = NULL;

	if (!strcmp(group_name, "/")) {
		path = (char*)malloc(group_length + name_length + 1); 
		strcat(strcpy(path, group_name), name);
	}
	else {
		path = (char*)malloc(group_length + name_length + 2);
		strcat(strcat(strcpy(path, group_name), "/"), name);
	}

	switch(obj_info.type) {
		case H5O_TYPE_GROUP:
			break;
		case H5O_TYPE_DATASET:
			svector_append(dsetnames, path);
			break;
		case H5O_TYPE_NAMED_DATATYPE:
			break;
		case H5O_TYPE_NTYPES:
			break;
		case H5O_TYPE_UNKNOWN:
			break;
	}

	// Clean up
	H5Oclose(obj);
	free(group_name);

	return SUCCEED;
}



/**
 * @brief 
 *
 * @param ifid
 * @param ofid
 * @param dsname
 *
 * @return 
 */
int copy_dataset(hid_t ifid, hid_t ofid, char* dsname) {
	printf("call %s\n", __PRETTY_FUNCTION__);
	assert(ifid > 0);
	assert(ofid > 0);
	assert(dsname != NULL);
	herr_t err;

	// Read dataset
	hid_t idid;
	hid_t mem_type_id;
	hid_t ispace;
	if ((idid = H5Dopen(ifid, dsname, H5P_DEFAULT)) < 0) BANG(1);
	if ((mem_type_id = H5Dget_type(idid)) < 0) BANG(1);
	if ((ispace = H5Dget_space(idid)) < 0) BANG(1);
	void* buffer = (void*)malloc(H5Dget_storage_size(idid));
	assert(buffer != NULL);
	if ((err = H5Dread(idid, mem_type_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, buffer)) < 0) BANG(err);

	/* EB: Creation of dataset with unlimited dimensions fails. This is a temporary workaround.
	 * It creates a new space with limited dimensions, and applies it to the new dataset.
	 */
	hid_t odid;
	hid_t idcpl;
	if ((idcpl = H5Dget_create_plist(idid)) < 0) BANG(1); // get dataset creation property list
	if ((err = H5Pset_attr_creation_order(idcpl, H5P_CRT_ORDER_TRACKED|H5P_CRT_ORDER_INDEXED)) < 0) BANG(1);
	if ((odid = H5Dcreate(ofid, dsname, mem_type_id, ispace, H5P_DEFAULT, idcpl, H5P_DEFAULT)) < 0) BANG(1);
	if ((err = H5Dwrite(odid, mem_type_id, ispace, ispace, H5P_DEFAULT, buffer)) < 0) BANG(err);
	
	// Clean up
	free(buffer);
	if ((err = H5Pclose(idcpl)) < 0) BANG(err);
	if ((err = H5Sclose(ispace)) < 0) BANG(err);
	if ((err = H5Dclose(idid)) < 0) BANG(err);
	if ((err = H5Dclose(odid)) < 0) BANG(err);
	if ((err = H5Tclose(mem_type_id)) < 0) BANG(err);

	return SUCCEED;
}



/**
 * @brief Program logic.
 *
 * @param p Initialization arguments.
 */
void run_stripper(param_t* p) {
	printf("call %s\n", __PRETTY_FUNCTION__);

	herr_t err;
	hid_t gcpl_id;
	gcpl_id = H5Pcreate(H5P_FILE_CREATE);
	H5Pset_link_creation_order(gcpl_id, H5P_CRT_ORDER_TRACKED|H5P_CRT_ORDER_INDEXED);
	H5Pset_attr_creation_order(gcpl_id, H5P_CRT_ORDER_TRACKED|H5P_CRT_ORDER_INDEXED);
	/*
	 * Create a file.
	 */
	hid_t ret;
	hid_t ifile;
	hid_t ofile;
	hid_t sfile;
	if ((ifile = H5Fopen(p->ifilename, H5F_ACC_RDONLY, H5P_DEFAULT)) < 0) BANG(1);
	if (p->ofilename != NULL) {ofile = H5Fcreate(p->ofilename, H5F_ACC_TRUNC, gcpl_id, H5P_DEFAULT);} else {ofile = -1;}
	if (p->sfilename != NULL) {sfile = H5Fcreate(p->sfilename, H5F_ACC_TRUNC, gcpl_id, H5P_DEFAULT);} else {sfile = -1;}

	svector_t dsetnames;
	svector_init(&dsetnames);
	get_dataset_names(ifile, &dsetnames);
	svector_print(&dsetnames);

//	svector_exclusion(dsetnames, sdsetnames, odsetnames);
//	svector_exclusion(dsetnames, odsetnames, sdsetnames);

	svector_t odsetnames;
	svector_t sdsetnames;
	svector_init(&odsetnames);
	svector_init(&sdsetnames);
	visitor2_param_t visitor2_params;


	// Logic control
	if (p->ofilename == NULL && p->sfilename == NULL) { // make full copy
		// do nothing
	}
	if (p->ofilename != NULL && p->sfilename == NULL) { // output only
		if (p->odsetnames.size != 0) {
			assert(sdsetnames.size == 0);
			svector_copy(&odsetnames, &p->odsetnames);
		}
		if (p->odsetnames.size == 0) {
			assert(sdsetnames.size == 0);
			svector_copy(&odsetnames, &dsetnames);
		}
	}
	if (p->ofilename == NULL && p->sfilename != NULL) { // shared only
		if (p->sdsetnames.size != 0) {
			assert(odsetnames.size == 0);
			svector_copy(&sdsetnames, &p->sdsetnames);
		}
		if (p->sdsetnames.size == 0) {
			assert(odsetnames.size == 0);
			svector_copy(&sdsetnames, &dsetnames);
		}
	}
	if (p->ofilename != NULL && p->sfilename != NULL) { // shared only
		if (p->odsetnames.size != 0 && p->sdsetnames.size != 0) {
			svector_copy(&odsetnames, &p->odsetnames);
			svector_copy(&sdsetnames, &p->sdsetnames);
		}
		if (p->odsetnames.size == 0 && p->sdsetnames.size != 0) {
			svector_exclude(&dsetnames, &p->sdsetnames, &odsetnames);
			svector_copy(&sdsetnames, &p->sdsetnames);
		}
		if (p->odsetnames.size != 0 && p->sdsetnames.size == 0) {
			svector_exclude(&dsetnames, &p->odsetnames, &sdsetnames);
			svector_copy(&odsetnames, &p->odsetnames);
		}
		if (p->odsetnames.size == 0 && p->sdsetnames.size == 0) {
			// nothing to do
		}
	}

	svector_print(&odsetnames);
	svector_print(&sdsetnames);

	// Copy root folder attributes
	visitor2_param_init(&visitor2_params, ifile, ofile, sfile, odsetnames, sdsetnames);
	if (err = H5Aiterate2(ifile, H5_INDEX_CRT_ORDER, H5_ITER_NATIVE, 0, visit_attributes, &visitor2_params) < 0) BANG(err);
	H5Lvisit(ifile, H5_INDEX_CRT_ORDER, H5_ITER_NATIVE, visitor3, (void*)&visitor2_params);	 // copy datasets
	H5Lvisit(ifile, H5_INDEX_CRT_ORDER, H5_ITER_NATIVE, visitor4, (void*)&visitor2_params);	 // fix references

	// Clean up
	if (ofile >= 0) H5Fclose(ofile);
	if (sfile >= 0) H5Fclose(sfile);
	H5Pclose(gcpl_id);
	H5Fclose(ifile);
	visitor2_param_destroy(&visitor2_params);
	svector_destroy(&dsetnames);
	svector_destroy(&odsetnames);
	svector_destroy(&sdsetnames);
}




/**
 * @brief Visit each object in the group and copy it to output and shared files.
 * Got through each object (datasets, ...) in the group.
 * Copy "extract" datasets to shared file
 * Copy "keep" datasets to output file
 *
 * @param gid group name in input file, containing dataset
 * @param name dataset name
 * @param info dataset information
 * @param opdata parameters
 *
 * @return 
 */
herr_t visitor3(hid_t gid, const char *name, const H5L_info_t *info, void *opdata) {
	herr_t err;
	visitor2_param_t* p = (visitor2_param_t*) opdata;
	char group_name[MAX_NAME];
	ssize_t len = H5Iget_name(gid, group_name, MAX_NAME);

	char* path = NULL;
	int group_length = strlen(group_name);
	int name_length = strlen(name);

	if (!strcmp(group_name, "/")) {
		printf("\t%s: group path: %s%s\n", __PRETTY_FUNCTION__, group_name, name);
		path = (typeof(path))malloc(group_length + name_length + 1); 
		strcat(strcpy(path, group_name), name);
	}
	else {
		printf("\t%s: group path: %s/%s\n", __PRETTY_FUNCTION__, group_name, name);
		path = (typeof(path))malloc(group_length + name_length + 2);
		strcat(strcat(strcpy(path, group_name), "/"), name);
	}

	hid_t gcpl_id;
	hid_t group;	
	hid_t obj = H5Oopen(gid, name, H5P_DEFAULT);
	H5O_info_t obj_info;
	H5Oget_info(obj, &obj_info);

	printf("\t%s: type: %s\n", __PRETTY_FUNCTION__, h5o_type_to_str(obj_info.type));
	switch(obj_info.type) {
		case H5O_TYPE_GROUP:
			gcpl_id = H5Pcreate(H5P_GROUP_CREATE);
			H5Pset_link_creation_order(gcpl_id, H5P_CRT_ORDER_TRACKED|H5P_CRT_ORDER_INDEXED);
			H5Pset_attr_creation_order(gcpl_id, H5P_CRT_ORDER_TRACKED|H5P_CRT_ORDER_INDEXED);
			if ((group = H5Gcreate(p->ofile, path, H5P_DEFAULT, gcpl_id, H5P_DEFAULT)) < 0) BANG(1);
			if ((err = H5Gclose(group)) < 0) BANG(err);
			break;
		case H5O_TYPE_DATASET:
			 printf("call %s\n", __PRETTY_FUNCTION__);
			 // Decide, where to copy the dataset
			 if (!svector_contains(&(p->sdsetnames), path)) {
				 printf("\t%s: copy dataset %s to output file\n", __PRETTY_FUNCTION__, path);
				 copy_dataset(p->ifile, p->ofile, path);
				 hid_t idataset;
				 printf("%s: create references of %s\n", __PRETTY_FUNCTION__, path);
				 if ((idataset = H5Dopen(p->ifile, path, H5P_DEFAULT)) < 0) BANG(1);
				 if (err = H5Aiterate2(idataset, H5_INDEX_CRT_ORDER, H5_ITER_NATIVE, 0, visit_attributes, opdata) < 0) BANG(err);
				 if (H5Dclose(idataset) < 0) BANG(1);
			 }	
			 else {
				 { // move this scope to a function
					 char* pch = strrchr(path, '/');
					 int gpath_len = pch - path;
					 char* gpath = NULL;
					 int gpath_size = gpath_len * sizeof(*gpath);
					 gpath = (typeof(gpath))malloc(gpath_size + 1);
					 memcpy(gpath, path, gpath_size);
					 gpath[gpath_size] = 0;
					 create_groups(p->sfile, gpath);
					 free(gpath);
				 }
				 copy_dataset(p->ifile, p->sfile, path);
				 create_group_link(p->sfile, (char*) name, p->ofile, (char*) name);
			 }
			break;
		case H5O_TYPE_NAMED_DATATYPE:
			break;
		case H5O_TYPE_NTYPES:
			break;
		case H5O_TYPE_UNKNOWN:
			break;
	}

	// Clean up
	return SUCCEED;
} 



/**
 * @brief 
 *
 * @param gid
 * @param name
 * @param info
 * @param opdata
 *
 * @return 
 */
herr_t visitor4(hid_t gid, const char *name, const H5L_info_t *info, void *opdata) {
	herr_t err;
	visitor2_param_t* p = (visitor2_param_t*) opdata;
	ssize_t name_length  = strlen(name);
	ssize_t group_length = H5Iget_name(gid,  NULL, 0);
	char* group_name = (typeof(group_name))malloc(sizeof(*group_name) * group_length + 1);
	H5Iget_name(gid, group_name, group_length);

	char* path = NULL;

	if (!strcmp(group_name, "/")) {
		path = (char*)malloc(group_length + name_length + 1); 
		strcat(strcpy(path, group_name), name);
	}
	else {
		path = (char*)malloc(group_length + name_length + 2);
		strcat(strcat(strcpy(path, group_name), "/"), name);
	}

	hid_t obj = H5Oopen(gid, name, H5P_DEFAULT);
	H5O_info_t obj_info;
	H5Oget_info(obj, &obj_info);

	switch(obj_info.type) {
		case H5O_TYPE_GROUP:
		case H5O_TYPE_DATASET:
			if (!svector_contains(&(p->sdsetnames), path)) { // fix references in output file
				hid_t idataset;
				if ((idataset = H5Dopen(p->ifile, path, H5P_DEFAULT)) < 0) BANG(1);
				if (err = H5Aiterate2(idataset, H5_INDEX_CRT_ORDER, H5_ITER_NATIVE, 0, visit_attributes_fix_refs, opdata) < 0) BANG(err);
				if (H5Dclose(idataset) < 0) BANG(1);
			}	
			else { // fix references in shared fiel
				// todo
			}
			break;
		case H5O_TYPE_NAMED_DATATYPE:
			break;
		case H5O_TYPE_NTYPES:
			break;
		case H5O_TYPE_UNKNOWN:
			break;
	}

	// Clean up
	free(path);
	free(group_name);
	return SUCCEED;
} 



/**
 * @brief Create netCDF compatible groups recursively
 *
 * @param loc
 * @param name
 *
 * @return 
 */
int create_groups(hid_t loc, char* rel_path) {
	// For netCDF compatibility
	hid_t gcpl_id = H5Pcreate(H5P_GROUP_CREATE);
	H5Pset_link_creation_order(gcpl_id, H5P_CRT_ORDER_TRACKED|H5P_CRT_ORDER_INDEXED);
	H5Pset_attr_creation_order(gcpl_id, H5P_CRT_ORDER_TRACKED|H5P_CRT_ORDER_INDEXED);

	// Tokenize path
	svector_t groups;
	svector_init(&groups);
	const char* delims = "/";
	tokenize(&groups, rel_path, delims);
	char* path = (typeof(path))malloc(sizeof(*path) * strlen(rel_path) + 1);
	path[0] = '\0';

	// Create path
	for (size_t i = 0; i < groups.size; ++i) {
		strcat(path, "/");
		strcat(path, groups.data[i]);
		if (H5Lexists(loc, path, H5P_DEFAULT) == false) {
			hid_t gid = H5Gcreate(loc, path, H5P_DEFAULT, gcpl_id, H5P_DEFAULT);
			H5Gclose(gid);
		}
	}

	// Clean up
	svector_destroy(&groups);
	free(path);
	H5Pclose(gcpl_id);
	return SUCCEED;
}



/**
 * @brief 
 *
 * @param ifid
 * @param igroupname
 * @param ofid
 * @param ogroupname
 *
 * @return 
 */
int create_group_link(hid_t ofid, char* dsname, hid_t ifid, char* linkname) {
	printf("call %s\n", __PRETTY_FUNCTION__);
	char filename[MAX_NAME];
	herr_t err;
	ssize_t len;
	if ((len = H5Fget_name(ofid, filename, MAX_NAME)) < 0) BANG(1);
	if ((err = H5Lcreate_external(filename, dsname, ifid, linkname, H5P_DEFAULT, H5P_DEFAULT)) < 0) BANG(err);
	return SUCCEED;
}



/**
 * @brief  Copy attribute (Iteration function)
 *
 * @param oid
 * @param name
 * @param info
 * @param opdata
 *
 * @return 
 */
herr_t visit_attributes(hid_t oid, const char* name, const H5A_info_t* info, void* opdata) {
	printf("call %s\n", __PRETTY_FUNCTION__);
	visitor2_param_t* p = (visitor2_param_t*) opdata;
	char* attr_path = NULL;
	char obj_name[MAX_NAME];
	ssize_t len = H5Iget_name(oid, obj_name, MAX_NAME);
	if (!strcmp(obj_name, "/")) {
		int size = strlen(obj_name) + strlen(name) + 1;
		attr_path = (char*)malloc(size);
		strcpy(attr_path, obj_name);
		strcat(attr_path, name);
	}
	else {
		int size = strlen(obj_name) + strlen(name) + 2;
		attr_path = (char*)malloc(size);
		strcpy(attr_path, obj_name);
		strcat(attr_path, "/");
		strcat(attr_path, name);
	}
	printf("\t%s: obj_name: %s; name: %s; obj path: %s\n", __PRETTY_FUNCTION__, obj_name, name, attr_path);

	copy_attribute(p->ifile, obj_name, p->ofile, obj_name, (char*) name);

	// Clean up
	free(attr_path);
	return 0;
}



/**
 * @brief 
 *
 * @param oid
 * @param name
 * @param info
 * @param opdata
 *
 * @return 
 */
herr_t visit_attributes_fix_refs(hid_t oid, const char* name, const H5A_info_t* info, void* opdata) {
	printf("call %s\n", __PRETTY_FUNCTION__);
	visitor2_param_t* p = (visitor2_param_t*) opdata;
	char* attr_path = NULL;
	char obj_name[MAX_NAME];
	ssize_t len = H5Iget_name(oid, obj_name, MAX_NAME);

	if (!strcmp(obj_name, "/")) {
		attr_path = (char*)malloc(strlen(obj_name) + strlen(name) + 1);
		strcat(strcpy(attr_path, obj_name), name);
	}
	else {
		attr_path = (char*)malloc(strlen(obj_name) + strlen(name) + 2);
		strcat(strcat(strcpy(attr_path, obj_name), "/"), name);
	}
	printf("\t%s: obj_name: %s; name: %s; obj path: %s\n", __PRETTY_FUNCTION__, obj_name, name, attr_path);
	fix_references(p->ifile, obj_name, p->ofile, obj_name, (char*) name); // start recursion
	free(attr_path);
	return 0;
}



/**
 * @brief 
 *
 * @param iloc
 * @param iname
 * @param oloc
 * @param oname
 * @param name
 *
 * @return 
 */
int copy_attribute (hid_t iloc, const char* iname, hid_t oloc, const char* oname, const char* name) {
	// Read attribute
	hid_t oid            = H5Oopen(iloc, iname, H5P_DEFAULT);
	hid_t attrid         = H5Aopen(oid, name, H5P_DEFAULT);
	hid_t attr_space     = H5Aget_space(attrid);
	hid_t attr_type      = H5Aget_type(attrid);
	hsize_t storage_size = H5Aget_storage_size(attrid);
	void* rdata = (void*)malloc(storage_size);
	if ((H5Aread(attrid, attr_type, rdata)) < 0) exit(1);
	H5Aclose(attrid);

	// Prepare output object
	ssize_t oobj_size = H5Iget_name(oid, NULL, 0);
	char* oobj_name = (typeof(oobj_name))malloc(sizeof(*(oobj_name)) * oobj_size + 1);
	H5Iget_name(oid, oobj_name,  oobj_size + 1);
	printf("\toobj_name: %s\n", oobj_name);
	hid_t oobj = H5Oopen(oloc, oname, H5P_DEFAULT);
	hid_t oattrid = H5Acreate(oobj, name, attr_type, attr_space, H5P_DEFAULT, H5P_DEFAULT);	

	// Write data
	if ((H5Awrite(oattrid, attr_type, rdata)) < 0) exit(1); // Use H5function to copy plain data

	// Clean up;
	H5Aclose(oattrid);
	H5Oclose(oobj);
	H5Oclose(oid);
	free(oobj_name);

	return SUCCEED;
}



/**
 * @brief Go recursively through all data types and set references similar to the original file.
 *
 * @param iloc input location
 * @param iname input dataset name
 * @param oloc output location
 * @param oname output dataset name
 * @param name attribute name
 *
 * @return SUCCEED if successful
 */
int fix_references (hid_t iloc, const char* iname, hid_t oloc, const char* oname, const char* name) {
		printf("call %s\n", __PRETTY_FUNCTION__);
		hid_t err;
		hid_t ioid  = H5Oopen(iloc, iname, H5P_DEFAULT);
		hid_t iaid  = H5Aopen(ioid, name, H5P_DEFAULT);
		hid_t itype = H5Aget_type(iaid);

		hid_t ooid  = H5Oopen(oloc, oname, H5P_DEFAULT);
		hid_t oaid  = H5Aopen(ooid, name, H5P_DEFAULT);
		hid_t otype = H5Aget_type(iaid);

		const int itotal_size = H5Aget_storage_size(iaid);
		const int ototal_size = H5Aget_storage_size(oaid);
		void* ibuf = (typeof(ibuf))malloc(sizeof(*ibuf) * itotal_size);
		void* obuf = (typeof(ibuf))malloc(sizeof(*obuf) * itotal_size);

		if (err = H5Aread(iaid, itype, ibuf)) BANG(err);
		if (err = H5Aread(oaid, otype, obuf)) BANG(err);

		fix(iloc, oloc, itype, itotal_size, ibuf, obuf);

		if (H5Awrite(oaid, otype, obuf) < 0) BANG(1);

		// Clean up
		H5Aclose(iaid);
		H5Oclose(ioid);
		H5Tclose(itype);
		H5Aclose(oaid);
		H5Oclose(ooid);
		H5Tclose(otype);
		free(ibuf);
		free(obuf);

		return SUCCEED;
}



/**
 * @brief Recursion for the fix_references function
 *
 * @param iloc
 * @param oloc
 * @param itype
 * @param buf_size
 * @param ibuf
 * @param obuf
 *
 * @return 
 */
int fix(hid_t iloc, hid_t oloc, hid_t itype, size_t buf_size, void* ibuf, void * obuf) {
		printf("call %s\n", __PRETTY_FUNCTION__);
		herr_t err;
		H5T_class_t class;
		hid_t super_type;
		H5T_class_t super_class;

		if ((class = H5Tget_class(itype)) == H5T_NO_CLASS) BANG(1);

		printf("\t%s class %s\n", __PRETTY_FUNCTION__, h5t_class_to_str(class));

		switch (class) {
			case H5T_INTEGER: break;
			case H5T_FLOAT: break;
			case H5T_STRING: break;
			case H5T_BITFIELD: break;
			case H5T_OPAQUE: break;
			case H5T_ARRAY: break;
			case H5T_ENUM: break;

			case H5T_REFERENCE:
				{
					hobj_ref_t* iref = (hobj_ref_t*)ibuf;
					hobj_ref_t* oref = (hobj_ref_t*)obuf;
					int reflen = H5Rget_name(iloc, H5R_OBJECT, (void*)ibuf,  NULL, 0);
					char* refname = (typeof(refname))malloc(sizeof(*(refname) + 1));
					H5Rget_name(iloc, H5R_OBJECT, (void*)ibuf,  refname, reflen + 1);
					// free(obuf); // ?
					printf("\t%s refname: %s\n", __PRETTY_FUNCTION__, refname);
					if ((err = H5Rcreate(obuf, oloc, refname, H5R_OBJECT, -1)) < 0) BANG(err);
					free(refname);	
				}
				break;

			case H5T_VLEN:
				{
					if ((super_type = H5Tget_super(itype)) < 0) BANG(1);
					if ((super_class = H5Tget_class(super_type)) == H5T_NO_CLASS) BANG(1);
					const int hvl_size = sizeof(hvl_t);
					const int ndims = buf_size / hvl_size; 
					hvl_t* iwdata = (hvl_t*)ibuf;
					hvl_t* owdata = (hvl_t*)obuf;
					for (int i = 0; i < ndims; ++i) {
						fix(iloc, oloc, super_type, hvl_size, (void*)iwdata[i].p, (void*)owdata[i].p); // recursion
					}
					if ((err = H5Tclose(super_type)) < 0) BANG(err);
				}
				break;

			case H5T_COMPOUND:
				{
					size_t isize = H5Tget_size(itype);
					int nelems = buf_size / isize;
					int nmembers = H5Tget_nmembers(itype);
					for (int j = 0; j < nelems; ++j) {
						for (int i = 0; i < nmembers; ++i) {
							int offset = H5Tget_member_offset(itype, i);
							hid_t memtype = H5Tget_member_type(itype, i);
							size_t memsize = H5Tget_size(memtype);
							fix(iloc, oloc, memtype, memsize, (void*)((char*)ibuf + offset + j*isize), (void*)((char*)obuf + offset + j*isize));
						}
					}
				}
				break;
			case H5T_NO_CLASS:
				BANG(1);
				break;
		}

		// Clean up

	return SUCCEED;
}




