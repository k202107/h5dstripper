#ifndef  hdf5basefuncs_INC
#define  hdf5basefuncs_INC

#include <hdf5.h>

const char* h5o_type_to_str(H5O_type_t value);
const char* h5i_type_to_str(H5T_class_t value);
const char* h5t_class_to_str(H5T_class_t value);
const char* h5s_sel_to_str(H5S_sel_type value);
const char* h5s_class_to_str(H5S_class_t value);
void print_space (hid_t spaceid);
void limit_dims(const int ndims, hsize_t* dims, hsize_t* maxdims);

#endif   /* ----- #ifndef hdf5basefuncs_INC  ----- */
