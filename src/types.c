#include "types.h"

/**
 * @brief 
 *
 * @param p paramter to initiate
 * @param ifile input file identifier
 * @param ofile ouput file identifier 
 * @param sfile shared file identifier
 * @param odsetnames output dataset names
 * @param sdsetnames shared dataset names
 */
void visitor2_param_init(visitor2_param_t* p,
		hid_t ifile, hid_t ofile, hid_t sfile, 
		svector_t odsetnames, svector_t sdsetnames) {
	p->ifile = ifile;
	p->ofile = ofile;
	p->sfile = sfile;
	svector_init(&(p->odsetnames));
	svector_init(&(p->sdsetnames));
	svector_copy(&(p->odsetnames), &odsetnames);
	svector_copy(&(p->sdsetnames), &sdsetnames);
}



/**
 * @brief Destroy initiated parameter to avoid memory leakage.
 *
 * @param p parameter to destroy
 */
void visitor2_param_destroy(visitor2_param_t* p) {
	svector_destroy(&(p->odsetnames));
	svector_destroy(&(p->sdsetnames));
}



/**
 * @brief Initiate command line Parameter
 *
 * @param p Paramter to initiate
 * @param ifilename input filename
 * @param ofilename output filename
 * @param sfilename shared filename
 * @param odsetnames output dataset names
 * @param sdsetnames shared dataset names
 */
void param_init(param_t* p,
		char* ifilename, char* ofilename, char* sfilename, 
		svector_t odsetnames, svector_t sdsetnames) {
	p->ifilename = ifilename;
	p->ofilename = ofilename;
	p->sfilename = sfilename;
	svector_init(&(p->odsetnames));
	svector_init(&(p->sdsetnames));
	svector_copy(&(p->odsetnames), &odsetnames);
	svector_copy(&(p->sdsetnames), &sdsetnames);
}



/**
 * @brief Destroy initiated parameter to avoid memory leakage.
 *
 * @param p paramter to destroy
 */
void param_destroy(param_t* p) {
	svector_destroy(&(p->odsetnames));
	svector_destroy(&(p->sdsetnames));
}



/**
 * @brief Print parameter
 *
 * @param p parameter to print
 */
void param_print(param_t* p) {
	printf("input file: \t%s\n", p->ifilename);
	if (p->ofilename) {
		printf("output file: \t%s\n", p->ofilename);
		if (p->odsetnames.size) {
			printf("datasets to keep:\n");
			for (int i = 0; i < p->odsetnames.size; ++i) {
				printf("\t%d\t%s\n", i, p->odsetnames.data[i]);
			}
		}
	}
	if (p->sfilename) {
		printf("shared file: \t%s\n", p->sfilename);
		printf("datasets to extract:\n");
		for (int i = 0; i < p->sdsetnames.size; ++i) {
			printf("\t%d\t%s\n", i, p->sdsetnames.data[i]);
		}
	}
}
